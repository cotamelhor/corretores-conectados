﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CorretoresConectados.Extensoes
{
    public interface IDbConnection
    {
        SqlConnection GetConnection();
    }
}
