﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorretoresConectados.Extensoes
{
    public static class DapperExtensions
    {
        public static void AddSqlDapper(this IServiceCollection services, IConfiguration configuration)
        {
            var section = configuration.GetSection("sqlconfig:ConnectionString");

            services.Configure<SqlConfig>(ppt => ppt.ConnectionString = section.Value);

            services.AddScoped<IDbConnection, DbConnection>();
        }
    }
}
