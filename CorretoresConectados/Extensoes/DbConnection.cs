﻿using Microsoft.Extensions.Options;
using System;
using System.Data.SqlClient;

namespace CorretoresConectados.Extensoes
{
    public class DbConnection : IDbConnection
    {
        private readonly string ConnectionString;

        public DbConnection(IOptions<SqlConfig> optConnectionString)
        {
            ConnectionString = optConnectionString?.Value?.ConnectionString ??
                throw new ArgumentException(nameof(optConnectionString));
        }

        public SqlConnection GetConnection()
        {
            var connection = new SqlConnection(ConnectionString);
            connection.Open();
            return connection;
        }
    }
}