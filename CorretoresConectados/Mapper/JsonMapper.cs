﻿using Newtonsoft.Json;

namespace CorretoresConectados.Mapper
{
    public static class JsonMapper
    {
        public static T Map<T>(this object @this)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(@this));
        }

        public static T Map<T>(this string @this)
        {
            return JsonConvert.DeserializeObject<T>(@this);
        }
    }
}