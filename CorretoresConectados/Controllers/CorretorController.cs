﻿using CorretoresConectados.Domain.Entidades;
using CorretoresConectados.Domain.Repositorios;
using CorretoresConectados.Domain.Servicos;
using CorretoresConectados.Mapper;
using CorretoresConectados.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CorretoresConectados.Controllers
{
    public class CorretorController : Controller
    {
        private readonly IServicoCorretor servicoCorretor;
        private readonly IRepositorioRepresentante repositorioRepresentante;

        public CorretorController(IServicoCorretor servicoCorretor, IRepositorioRepresentante repositorioRepresentante)
        {
            this.servicoCorretor = servicoCorretor;
            this.repositorioRepresentante = repositorioRepresentante;
        }

        public IActionResult Index([FromQuery] string codigo)
        {
            
            ViewBag.Codigo = codigo;
            return View();
            //return RedirectToAction("Index", "Jornada"); 
        }

        public IActionResult Inicial()
        {
            return View("Index");
        }

        public IActionResult Direto([FromQuery] string codigo)
        {
            ViewBag.Codigo = codigo;
            return View();
        }

        public IActionResult Pagamento([FromQuery] string codigo)
        {
            ViewBag.Codigo = codigo;
            return View();
        }

        public IActionResult Inscricao([FromQuery] string codigo)
        {
            ViewBag.Codigo = codigo;
            return View("Pagamento");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> AddAsync(CorretorViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //var representante = repositorioRepresentante
                    //    .BuscarPorAsync(model.Codigo)
                    //    .GetAwaiter()
                    //    .GetResult();

                    var corretor = model.Map<Corretor>();

                    //corretor.RepresentanteId = representante?.Id;

                    corretor = await servicoCorretor.AdicionarAsync(corretor).ConfigureAwait(false);

                    //if (corretor?.Id != Guid.Empty)
                    //{
                    //    //if (representante != null)
                    //    //{
                    //    //    await repositorioRepresentante
                    //    //        .AutoResponderCorretorAsync(corretor).ConfigureAwait(false);
                    //    //}
                    //    await repositorioRepresentante.AutoResponderCorretorAsync(corretor).ConfigureAwait(false);
                    //    return Json(new { success = true });
                    //}

                    return Json(new { success = true });
                }

                if (ModelState.ErrorCount > 0)
                {
                    var errors = string.Empty;
                    foreach (var key in ViewData.ModelState.Keys)
                    {
                        if (ModelState[key].Errors.Count > 0)
                        {
                            var modelError = ModelState[key].Errors[0];
                            return BadRequest(new
                            {
                                field = key,
                                message = modelError.ErrorMessage
                            });
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                return BadRequest();
            }

            return Ok();
        }

        public IActionResult Obrigado() => View();
    }
}