﻿using System;
using System.Threading.Tasks;
using CorretoresConectados.Domain.Servicos;
using CorretoresConectados.Models;
using Microsoft.AspNetCore.Mvc;
using CorretoresConectados.Mapper;
using CorretoresConectados.Domain.Entidades;
using CorretoresConectados.Domain.Repositorios;

namespace CorretoresConectados.Controllers
{
    public class JornadaController : Controller
    {
        private readonly IServicoCorretorJornada servicoCorretorJornada;
        private readonly IRepositorioRepresentante repositorioRepresentante;

        public JornadaController(IServicoCorretorJornada servicoCorretorJornada, IRepositorioRepresentante repositorioRepresentante)
        {
            this.servicoCorretorJornada = servicoCorretorJornada;
            this.repositorioRepresentante = repositorioRepresentante;
        }

        public IActionResult Index([FromQuery] string codigo)
        {
            ViewBag.Codigo = codigo;
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> AddAsync(JornadaViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var representante = repositorioRepresentante
                        .BuscarPorAsync(model.Codigo)
                        .GetAwaiter()
                        .GetResult();

                    var corretor = model.Map<CorretorJornada>();

                    corretor.RepresentanteId = representante?.Id;
                    corretor.CriadoEm = DateTime.Now;
                    corretor.NumeroEndereco = model.Numero;
                    corretor.WhatsApp = corretor.WhatsApp.Replace("(", "").Replace(")", "").Replace("-", "");
                    corretor = await servicoCorretorJornada.AdicionarAsync(corretor).ConfigureAwait(false);

                    if (corretor?.Id != Guid.Empty)
                    {
                        return Json(new { success = true });
                    }
                }

                if (ModelState.ErrorCount > 0)
                {
                    var errors = string.Empty;
                    foreach (var key in ViewData.ModelState.Keys)
                    {
                        if (ModelState[key].Errors.Count > 0)
                        {
                            var modelError = ModelState[key].Errors[0];
                            return BadRequest(new
                            {
                                field = key,
                                message = modelError.ErrorMessage
                            });
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                return BadRequest();
            }

            return RedirectPermanent("https://link.pagar.me/lryzjyUXFB");
        }


    }
}