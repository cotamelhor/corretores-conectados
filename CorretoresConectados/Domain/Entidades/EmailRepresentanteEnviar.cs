﻿using System;

namespace CorretoresConectados.Domain.Entidades
{
    public class EmailRepresentanteEnviar
    {
        public Guid EmailId { get; set; }
        public Guid RepresentanteId { get; set; }
    }
}