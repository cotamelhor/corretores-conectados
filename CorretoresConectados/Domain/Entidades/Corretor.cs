﻿using Dapper;
using System;

namespace CorretoresConectados.Domain.Entidades
{
    [Table(nameof(CorretoresConectados))]
    public class Corretor : EntidadeBase
    {
        public string Nome { get; set; }

        public string WhatsApp { get; set; }

        public string Email { get; set; }

        public string CEP { get; set; }

        public string Creci { get; set; }
        public string TipoCorretor { get; set; }

        public string CPF { get; set; }

        public Guid? RepresentanteId { get; set; }

        public void TratarValores()
        {
            if (!string.IsNullOrEmpty(CEP))
            {
                CEP = CEP.Replace("-", "");
            }
            if (!string.IsNullOrEmpty(WhatsApp))
            {
                WhatsApp = WhatsApp
                   .Replace("(", "")
                   .Replace(")", "")
                   .Replace("-", "");
            }
            if (!string.IsNullOrEmpty(CPF))
            {
                CPF = CPF
                    .Replace(".", "")
                    .Replace("-", "");
            }

            if (nameof(Corretor).Equals(TipoCorretor))
            {
                CPF = null;
            }
            else
            {
                Creci = null;
            }
        }
    }
}