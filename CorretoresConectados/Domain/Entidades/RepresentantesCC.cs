﻿using System;

namespace CorretoresConectados.Domain.Entidades
{
    public class RepresentanteCC : EntidadeBase
    {
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Codigo { get; set; }
    }
}