﻿using Dapper;
using System;

namespace CorretoresConectados.Domain.Entidades
{
    public abstract class EntidadeBase
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime CriadoEm { get; set; }

        protected EntidadeBase()
        {
            CriadoEm = DateTime.Now;
        }
    }
}