﻿using Dapper;
using System;

namespace CorretoresConectados.Domain.Entidades
{
    [Table("CorretoresConectadosJornada")]
    public class CorretorJornada : EntidadeBase
    {
        public string Nome { get; set; }
        public string WhatsApp { get; set; }
        public string Email { get; set; }
        public string CEP { get; set; }
        public string Endereco { get; set; }
        public string CPF { get; set; }
        public int NumeroEndereco { get; set; }
        public int Parcelas { get; set; }
        public string Meio { get; set; }
        public Guid? RepresentanteId { get; set; }
    }
}