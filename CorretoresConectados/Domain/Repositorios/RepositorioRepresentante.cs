﻿using CorretoresConectados.Domain.Entidades;
using CorretoresConectados.Extensoes;
using CorretoresConectados.Mapper;
using CorretoresConectados.Models;
using Dapper;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CorretoresConectados.Domain.Repositorios
{
    public class RepositorioRepresentante : Repositorio<RepresentanteCC>, IRepositorioRepresentante
    {
        private readonly IDbConnection dbConnection;
        private readonly string autoresponerUrl;

        public RepositorioRepresentante(Extensoes.IDbConnection dbConnection, IOptions<AutoResponderConfig> options) : base(dbConnection)
        {
            this.dbConnection = dbConnection;
            this.autoresponerUrl = options.Value.Url;
        }

        public async Task AutoResponderCorretorAsync(Corretor corretor)
        {
            using (var client = new HttpClient())
            {
                var response = client
                    .GetAsync($"{autoresponerUrl}/fluxo?description=corretores-conectados")
                    .GetAwaiter()
                    .GetResult();

                var fluxo = (await response.Content.ReadAsStringAsync())
                    .Map<FluxoResponse>();

                if (fluxo == null)
                {
                    return;
                }
                var request = new
                {
                    FluxoId = fluxo.Id.ToString(),
                    corretor.Nome,
                    telefone = corretor.WhatsApp,
                    corretor.Email,
                    corretor.RepresentanteId
                };

                var content = JsonConvert.SerializeObject(request);

                using (var payload = new StringContent(content, Encoding.UTF8, "application/json"))
                {
                    var envioResponse = await client.PostAsync($"{autoresponerUrl}/corretores-conectados/fluxo", payload)
                                            .ConfigureAwait(false);
                }
            }
        }

        public async Task<RepresentanteCC> BuscarPorAsync(string codigo)
        {
            using (var connection = dbConnection.GetConnection())
            {
                const string where = "Where codigo = @codigo";
                var result = await connection
                    .GetListAsync<RepresentanteCC>(where, new { codigo })
                        .ConfigureAwait(false);
                return result.FirstOrDefault();
            }
        }
    }
}