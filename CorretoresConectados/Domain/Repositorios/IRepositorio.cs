﻿using CorretoresConectados.Domain.Entidades;
using System;
using System.Threading.Tasks;

namespace CorretoresConectados.Domain.Repositorios
{
    public interface IRepositorio<T> where T : EntidadeBase
    {
        Task<T> AdicionarAsync(T entidade);

        Task<bool> Existe(Guid id);
    }
}