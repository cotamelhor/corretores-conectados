﻿using CorretoresConectados.Domain.Entidades;
using CorretoresConectados.Extensoes;
using Dapper;
using System;
using System.Threading.Tasks;

namespace CorretoresConectados.Domain.Repositorios
{
    public class Repositorio<T> : IRepositorio<T> where T : EntidadeBase
    {
        private readonly IDbConnection dbConnection;

        public Repositorio(IDbConnection dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        public async Task<T> AdicionarAsync(T entidade)
        {
            using (var connection = dbConnection.GetConnection())
            {
                entidade.Id = await connection.InsertAsync<Guid, T>(entidade).ConfigureAwait(false);

                return entidade;
            }
        }

        public async Task<bool> Existe(Guid id)
        {
            using (var connection = dbConnection.GetConnection())
            {
                var entidade = await connection.GetAsync<T>(id).ConfigureAwait(false);

                return entidade != null;
            }
        }
    }
}