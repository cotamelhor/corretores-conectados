﻿using CorretoresConectados.Domain.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CorretoresConectados.Domain.Repositorios
{
    public interface IRepositorioRepresentante : IRepositorio<RepresentanteCC>
    {
        Task<RepresentanteCC> BuscarPorAsync(string codigo);
        Task AutoResponderCorretorAsync(Corretor corretor);
    }
}
