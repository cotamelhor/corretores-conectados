﻿using CorretoresConectados.Domain.Entidades;
using CorretoresConectados.Domain.Repositorios;
using System.Threading.Tasks;

namespace CorretoresConectados.Domain.Servicos
{
    public class Servico<T> : IServico<T> where T : EntidadeBase
    {
        private readonly IRepositorio<T> repositorio;

        public Servico(IRepositorio<T> repositorio)
        {
            this.repositorio = repositorio;
        }

        public Task<T> AdicionarAsync(T entidade)
        => repositorio.AdicionarAsync(entidade);
    }
}