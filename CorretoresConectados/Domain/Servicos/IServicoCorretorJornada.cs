﻿using CorretoresConectados.Domain.Entidades;

namespace CorretoresConectados.Domain.Servicos
{
    public interface IServicoCorretorJornada : IServico<CorretorJornada>
    {
        
    }
}