﻿using CorretoresConectados.Domain.Entidades;

namespace CorretoresConectados.Domain.Servicos
{
    public interface IServicoCorretor : IServico<Corretor>
    {
        
    }
}