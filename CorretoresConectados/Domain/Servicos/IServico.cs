﻿using CorretoresConectados.Domain.Entidades;
using System.Threading.Tasks;

namespace CorretoresConectados.Domain.Servicos
{
    public interface IServico<T> where T : EntidadeBase
    {
        Task<T> AdicionarAsync(T entidade);
    }
}