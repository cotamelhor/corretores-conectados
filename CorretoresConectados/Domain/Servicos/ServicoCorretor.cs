﻿using CorretoresConectados.Domain.Entidades;
using CorretoresConectados.Domain.Repositorios;
using System.Threading.Tasks;

namespace CorretoresConectados.Domain.Servicos
{
    public class ServicoCorretor : IServicoCorretor
    {
        private readonly IRepositorio<Corretor> servico;
        private readonly IRepositorioRepresentante repositorioRepresentante;

        public ServicoCorretor(IRepositorio<Corretor> servico, IRepositorioRepresentante repositorioRepresentante)
        {
            this.servico = servico;
            this.repositorioRepresentante = repositorioRepresentante;
        }

        public Task<Corretor> AdicionarAsync(Corretor entidade)
        {
            entidade.TratarValores();

            return servico.AdicionarAsync(entidade);
        }
    }
}