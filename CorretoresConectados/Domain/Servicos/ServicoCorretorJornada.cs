﻿using CorretoresConectados.Domain.Entidades;
using CorretoresConectados.Domain.Repositorios;
using System.Threading.Tasks;

namespace CorretoresConectados.Domain.Servicos
{
    public class ServicoCorretorJornada : IServicoCorretorJornada
    {
        private readonly IRepositorio<CorretorJornada> servico;

        public ServicoCorretorJornada(IRepositorio<CorretorJornada> servico)
        {
            this.servico = servico;
        }

        public Task<CorretorJornada> AdicionarAsync(CorretorJornada entidade)
        {
            return servico.AdicionarAsync(entidade);
        }
    }
}