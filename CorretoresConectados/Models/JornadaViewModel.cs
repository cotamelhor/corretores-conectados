﻿using System.ComponentModel.DataAnnotations;

namespace CorretoresConectados.Models
{
    public class JornadaViewModel
    {
        [Required(ErrorMessage = "Este campo é Requerido!")]
        [MaxLength(151, ErrorMessage = "no máximo 150 letras.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Este campo é Requerido!")]
        [DataType(DataType.PhoneNumber)]
        public string WhatsApp { get; set; }

        public string Email { get; set; }

        public string CEP { get; set; }
        public string Endereco { get; set; }
        public string CPF { get; set; }
        public int Numero { get; set; }
        public int Parcelas { get; set; }
        public string Meio { get; set; }
        public string Codigo { get; set; }

    }
}