﻿using System;

namespace CorretoresConectados.Models
{
    public class FluxoResponse
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
    }
}