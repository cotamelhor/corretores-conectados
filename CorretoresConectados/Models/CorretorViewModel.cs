﻿using System.ComponentModel.DataAnnotations;

namespace CorretoresConectados.Models
{
    public class CorretorViewModel
    {
        [Required(ErrorMessage = "Este campo é Requerido!")]
        [MaxLength(151, ErrorMessage = "no máximo 150 letras.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Este campo é Requerido!")]
        [DataType(DataType.PhoneNumber)]
        public string WhatsApp { get; set; }

        [Required(ErrorMessage = "Este campo é Requerido!")]
        [MaxLength(151, ErrorMessage = "no máximo 150 letras.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string Codigo { get; set; }

        public string CEP { get; set; }

        public string TipoCorretor { get; set; }
        public string Creci { get; set; }
    }
}